CXXFLAGS=-ggdb -g3
lfsr: wrap.c lfsr.o
	gcc $(CXXFLAGS) -o lfsr lfsr.o wrap.c
	rm lfsr.o

lfsr.o: lfsr.s
	nasm -felf64 lfsr.s
