BITS 64

global get_byte
global get_byte2
global polinom
global reg

%define mask 0x8000000000000000

section .bss

reg resq 1
polinom resq 1

section .text

get_byte:
push rbx
push rcx
push rdx
push rdi
push rsi
push r8


xor al, al
mov rcx, 8
mov r8, mask

mov rsi, [reg]
mov rdi, [polinom]

.lp1:
xor dl, dl
mov rbx, rsi
and rbx, rdi
.lp:
shr rbx,1
jnc .if1
xor dl, 1
.if1:
test rbx, rbx
jnz .lp
shl al, 1
shr rsi, 1
jnc .if2
or al, 1
.if2:
test dl, dl
jz .if3
or rsi, r8
.if3:
loop .lp1

mov [reg], rsi

pop r8
pop rsi
pop rdi
pop rdx
pop rcx
pop rbx
ret

get_byte2:
push rcx
push rdi
push rsi
push r8


xor rax, rax
mov rcx, 8

mov rsi, [reg]
mov rdi, [polinom]

.lp:
shl al, 1
shr rsi, 1
jnc .lp1
xor rsi, rdi
or al, 1
.lp1:
loop .lp

mov [reg], rsi

pop r8
pop rsi
pop rdi
pop rcx
ret

