#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

typedef unsigned long long base;
typedef unsigned char uchar;

extern base polinom;
extern base reg;
extern uchar get_byte();
extern uchar get_byte2();

base hex2base(uchar * str){
	int i, n;
	base res;
	res = 0;
	n = strlen(str);
	for (i = 0; i < n && i < 2 * sizeof(base); i++) {
		if(str[i] >= '0'){
			if(str[i] <= '9'){
				res = (res << 4) | (str[i] - '0');
			} else if (str[i] <= 'f' && str[i] >= 'a'){
				res = (res << 4) | (str[i] - 'a' + 10);
			} else return 0;
		} else {
			return 0;
		}
	}
	return res;
}

base reverse_base(base b){
	int i, n;
	base mask, res;
	n = sizeof(base) * 8;
	for(i = 0, mask = 1, res = 0; i < n; i++, b >>= 1){
		res = (res << 1) | (mask & b);
	}
	return res;
}

int main(int argc, char * argv[]){
	int n, i, v;
	uchar (*f)(void);
	uchar c;
	if (argc != 5) {
		puts("USAGE; ./lfsr hex_polinom hex_init num_of_bytes version\n");
		return -1;
	}
	v = atoi(argv[4]);
	if(v != 1 && v != 2){
		return -1;
	} else {
		if(v == 1){
			f = get_byte;
		} else f = get_byte2;
	}
	polinom = hex2base(argv[1]);
	if(v == 1){
		polinom = reverse_base(polinom);
	}
	reg = hex2base(argv[2]);
	n = atoi(argv[3]);
	if(n < 0 || !reg || !polinom){
		return -1;
	}
	for(i = 0; i < n; i++){
		c = f();
		write(fileno(stdout), (char *)&c, 1);
	}
	return 0;
}
